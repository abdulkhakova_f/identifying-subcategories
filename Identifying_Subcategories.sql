WITH SubcategorySales AS (
  SELECT
    sub.category,
    year,
    SUM(sales) AS amount_sold
  FROM
    sh.sales s
  WHERE
    year IN (1998, 1999, 2000, 2001)
  GROUP BY
    subcategory, year
),
 end_of_cal_year AS (
  SELECT
    subcategory,
    year,
    SUM(sales) AS end_of_cal_year
  FROM
    your_sales_table
  WHERE
    year = 1997
  GROUP BY
    subcategory, year
)
SELECT
  s.subcategory,
  sales.year,
  s.total_sales,
  p.previous_year_sales
FROM
  SubcategorySales s
JOIN
  PreviousYearSales p ON s.subcategory = p.subcategory AND s.year = p.year + 1
WHERE
  s.total_sales > p.previous_year_sales;
